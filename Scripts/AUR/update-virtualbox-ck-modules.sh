# re-install the package
yay -S virtualbox-ck-host-modules --noconfirm
# --nodiffmenu --noeditmenu --nouseask --nocleanmenu --noupgrademenu

# Unload older modules
sudo modprobe -r vboxvideo vboxpci vboxnetadp vboxnetflt vboxguest
sudo modprobe -r vboxdrv

# Start the service
sudo systemctl start systemd-modules-load.service 

# Get the logs
# Notes to future version: grep only what I need
systemctl status systemd-modules-load.service --no-pager
